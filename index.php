<?php

use App\Controller\OrderController;
use App\Database\Database;
use App\DependencyInjection\LoggerCompilerPass;
use App\Logger;
use App\Mailer\GmailMailer;
use App\Mailer\SmtpMailer;
use App\Texter\FaxTexter;
use App\Texter\SmsTexter;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

require __DIR__ . '/vendor/autoload.php';

$container = new ContainerBuilder();

$container->setParameter('mailer.gmail_user', 'lior@gmail.com');
$container->setParameter('mailer.gmail_password', '123456');

/* $controllerDefinition = new Definition(OrderController::class, [
    new Reference('database'),
    new Reference('mailer.gmail'),
    new Reference('texter.sms')
]); */
/* $controllerDefinition
->addMethodCall('sayHello', [
    'Bonjour à tous',
    65
])
->addMethodCall('setSecondaryMailer', [
    new Reference('mailer.gmail')
])
; */
//$container->setDefinition('order_controller', $controllerDefinition);

// la metode autowire permet de remplacer la méthode register et permet de ne pas mettre de setAutowired(true)
$container->autowire('order_controller', OrderController::class)
    ->setPublic(true)
    // le setAutowired permet que lorsuq'on declare le service, le container comprend les paramètres a utilisé qui sont dans le constructeur de la classe en question. l'autowiring ne fonctionne seulement si le conteneur est compilé
    //->setAutowired(true)
    // ->setArguments([
    //     new Reference(Database::class),
    //     new Reference(GmailMailer::class),
    //     new Reference(SmsTexter::class)
    // ])
    ->addMethodCall('sayHello', [
        'Bonjour à tous',
        65
    ])
    ->addMethodCall('setSecondaryMailer', [
        new Reference('mailer.gmail')
    ])
    ;

/* $databaseDefinition = new Definition(Database::class);
$container->setDefinition('database',$databaseDefinition); */

//$container->register('database', Database::class)->setAutowired(true);
// Le code ci dessus peut être écrit de la manière ci-dessous
$container->autowire('database', Database::class);

/* $smsTexterDefinition = new Definition(SmsTexter::class, [
    'service.sms.com',
    'apikey123'
]); */
/* $smsTexterDefinition
    ->addArgument("service.sms.com")
    ->addArgument("apikey123"); */
/* $smsTexterDefinition->setArguments([
    'service.sms.com',
    'apikey123'
]); */
//$container->setDefinition("texter.sms", $smsTexterDefinition);
$container->register('texter.sms', SmsTexter::class)
    ->setAutowired(true)
    ->setArguments([
        'service.sms.com',
        'apikey123'
    ])
    ->addTag('with_logger');

/* $gmailMailerDefinition = new Definition(GmailMailer::class, [
    'lior@gmail.com',
    '123456'
]);
$container->setDefinition('mailer.gmail', $gmailMailerDefinition); */
$container->register('mailer.gmail', GmailMailer::class)
    ->setAutowired(true)
    ->setArguments([
        '%mailer.gmail_user%',
        '%mailer.gmail_password%'
    ])
    ->addTag('with_logger');

//$database = new Database();
//$database = $container->get('database');

//$texter = new SmsTexter("service.sms.com", "apikey123");
//$texter = $container->get("texter.sms");

//$mailer = new GmailMailer("lior@gmail.com", "123456");
//$mailer = $container->get('mailer.gmail');

$container->register('mailer.smtp', SmtpMailer::class)
        ->setAutowired(true)
        ->setArguments([
            'smtp://localhost',
            'root',
            '123'
        ]);

$container->register('texter.fax', FaxTexter::class)->setAutowired(true);

$container->register('logger', Logger::class)->setAutowired(true);

$container->setAlias('App\Controller\OrderController', 'order_controller')->setPublic(true);
$container->setAlias('App\Database\Database', 'database');
$container->setAlias('App\Mailer\GmailMailer', 'mailer.gmail');
$container->setAlias('App\Texter\SmsTexter', 'texter.sms');
$container->setAlias('App\Mailer\SmtpMailer', 'mailer.smtp');
$container->setAlias('App\Texter\FaxTexter', 'texter.fax');
$container->setAlias('App\Mailer\MailerInterface', 'mailer.gmail');
$container->setAlias('App\Texter\TexterInterface', 'texter.sms');
$container->setAlias('App\Logger', 'logger');

$container->addCompilerPass(new LoggerCompilerPass);

$container->compile();

//$controller = new OrderController($database, $mailer, $texter);
$controller = $container->get(OrderController::class);

$httpMethod = $_SERVER['REQUEST_METHOD'];

if($httpMethod === 'POST') {
    $controller->placeOrder();
    return;
}

include __DIR__. '/views/form.html.php';
